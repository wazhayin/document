登录用户在个人设置页面可以修改自己的密码，修改过密码之后，自动登出其他session。

# 修改密码

**URL** `/user/change-password/`

**http请求** `POST`

**Request body**
```json
{
  "password": "old_password",
  "new_password": "new_password",
  "new_password_r": "new_password_repeat"
}
```

**Response**

```json
{
  "code": "OK",
  "msg": "成功",
  "data": {
    "uid": 3,
    "username": "username",
    "email": "email@explem.com",
    "email_verified": true,
    "create_time": "2018-06-13 16:37:49",
    "last_login": 1529910977,
    "last_login_ip": "127.0.0.1"
  }
}
```

**字段说明**
 
| Param | Type | Description | Required? |
| --- | --- | --- | --- |
| password | `String` | 旧密码 | *required |
| new_password | `String` | 新密码 | *required |
| new_password_r | `String` | 重复新密码 | *required |

