
## 用户重设密码

前端重设密码页面的路由为：`/user/reset-password?id=xxxxxxxxxx&token=xxxxxxxxxx`

用户输入两次新密码后，将用户输入的内容及 path 中的 `id` 和 `token` 字段发送给服务器。

**URL** `/user/reset-password/`

**HTTP请求方式** `POST`

**Request body**
```json
{
  "password": "pas5sW0rd1",
  "password_r": "pas5sW0rd1",
  "id": "xxxxxxxxx",
  "token": "xxxxxxxxxxxxx"
}
```

**Response**

```json
{
  "code": "OK",
  "msg": "成功"
}
```

**字段说明**

| Param | Type | Description | Required? |
| --- | --- | --- | --- |
| password | `String` | 密码 | *required |
| password_r | `String` | 重复密码 | *required |
| id | `String` | path 中的 id 参数 | *required |
| token | `String` | path 中的 token 参数 | *required |
