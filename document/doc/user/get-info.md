# 用户信息获取

**URL**  `/user/profile/`

**HTTP请求方式** `GET`

**Response**
```json
{
  "code": "OK",
  "msg": "成功",
  "data": {
    "uid": 3,
    "username": "username",
    "email": "email@explem.com",
    "email_verified": true,
    "create_time": "2018-06-13 16:37:49",
    "last_login": 1529910977,
    "last_login_ip": "127.0.0.1"
  }
}
```