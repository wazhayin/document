# 验证邮箱

在用户注册后，系统会向用户填写的 email 地址中发送一封注册验证邮件，其中验证链接的 path 为：
`/user/verify-email?id=<XXXXXX>&token=<XXXXXX>`。

用户点击链接进入网站，在该路由下，前端代码应获取 URL 中的 `id` 和 `token` 字段，向后端发送请求，验证其有效性。


**URL** ` /user/verify-email/`

**HTTP请求方式** `POST`

**Request body**
```json
{
  "id": "xxxxx",
  "token": "xxxxx"
}
```

**Response**
```json
{
  "code": "OK",
  "msg": "成功"
}
```

**字段说明**

| Param | Type | Description | Required? |
| --- | --- | --- | --- |
| id | `String` | 用户id | *required |
| token | `String` | token | *required |



