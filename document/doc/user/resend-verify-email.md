# 重发验证邮件

**URL** `/user/resend-verify-email/`

**HTTP请求方式** `POST`

**Request body**

空body

**Response**
```json
{
  "code": "OK",
  "msg": "成功"
}
```

**字段说明**



- 如果服务器返回用户未登录，应跳转到登录界面让用户登录。
- 重发验证邮件的接口后台进行了频率限制，登录用户，每IP每分钟最多2次请求。