# 用户登陆

**URL** `/user/login/`

**HTTP请求方式** `POST`

**Request body**
```json
{
    "username": "username",
    "password": "pas5Word"

}
```

**Response**
```json
{ 
  "code": "OK",
  "msg": "成功",
  "data": {
    "uid": 3,
    "username": "username",
    "email": "email@explem.com",
    "email_verified": true,
    "create_time": "2018-06-13 16:37:49",
    "last_login": 1529910977,
    "last_login_ip": "127.0.0.1"
  }
}
```

 **字段说明**
 
| Param | Type | Description | Required? |
| --- | --- | --- | --- |
| username | `String` | 用户名 | *required |
| password | `String` | 密码 | *required |