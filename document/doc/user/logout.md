# 用户登出

**URL** `/user/logout/`

**HTTP请求方式** `POST`

**Request body**

空body

**Response**
```json
{
  "code": "OK",
  "msg": "成功"
}
```