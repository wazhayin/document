登录用户在个人设置页面可以修改自己的用户名。新的用户名不能与其他用户的用户名重复；修改过密码之后，自动登出其他session。

# 修改用户名

**URL** `/user/change-username/`

**http请求** `POST`

**Request body**
```json
{
  "username": "new_fancy_username"
}
```

**Response**

```json
{
  "code": "OK",
  "msg": "成功",
  "data": {
    "uid": 3,
    "username": "username",
    "email": "email@explem.com",
    "email_verified": true,
    "create_time": "2018-06-13 16:37:49",
    "last_login": 1529910977,
    "last_login_ip": "127.0.0.1"
  }
}
```

 **字段说明**
 
| Param | Type | Description | Required? |
| --- | --- | --- | --- |
| username | `String` | 用户名 | *required |