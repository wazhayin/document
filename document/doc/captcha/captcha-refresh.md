# 刷新验证码

**URL** `/captcha-refresh/`

**HTTP请求方式** `GET`


**Response**
```json
{
  "code": "OK",
  "msg": "成功",
  "data": {
    image_url:"/captcha/image/a1abd026e30ceb1b5164f4ada078981d11f2357a/",
    key: "a1abd026e30ceb1b5164f4ada078981d11f2357a"
  }
}
```