# 管理员登出

**URL** `/staff/logout/`

**HTTP请求方式** `POST`

**Request body**

空body

**Response**
```json
{
  "code": "OK",
  "msg": "成功"
}
```