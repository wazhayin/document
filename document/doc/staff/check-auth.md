# 管理员登陆状态检查

**URL** `/staff/check-auth/`

**HTTP请求方式** GET


**Response**
```json
{
  "code": "OK",
  "msg": "成功",
  "data": {
    "username": "staff",
    "email": "staff@test.com",
    "permissions": "super_admin"
  }
}
```